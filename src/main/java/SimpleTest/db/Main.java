package SimpleTest.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Main {

    private final static String URL = "jdbc:postgresql://localhost:5432/mydbtest";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "postgres";

    public static void main(String[] args) throws SQLException {

        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        if (!connection.isClosed()) {
            System.out.println("Соединение с БД Установлено!");
        }
        connection.close();
        if (connection.isClosed()) {
            System.out.println("Соединение с БД Закрыто!");
        }
    }
}